export default {
  mode: 'spa',
  /*
  ** Headers of the page
  */
  head: {
    titleTemplate(titleChunk) {
      return titleChunk ? `${titleChunk} - DataGit` : 'DataGit';
    },
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width,initial-scale=1' },
      { hid: 'description', name: 'description', content: process.env.npm_package_description || '' },
    ],
  },
  /*
  ** Disable the progress-bar
  */
  loading: false,
  /*
  ** Global CSS
  */
  css: [
  ],
  /*
  ** Plugins to load before mounting the App
  */
  plugins: [
    '~/plugins/vue-clipboard2',
  ],
  /*
  ** Nuxt.js dev-modules
  */
  buildModules: [
    '@nuxtjs/eslint-module',
    '@nuxtjs/vuetify',
  ],
  /*
  ** Nuxt.js modules
  */
  modules: [
  ],
  /*
  ** vuetify module configuration
  */
  vuetify: {},
  /*
  ** Build configuration
  */
  build: {
    extend(config, ctx) {
    },
  },
  /*
  ** Generate configuration
  */
  generate: {
    dir: 'public',
    fallback: true,
  },
  /*
  ** Router configuration
  */
  router: {
    trailingSlash: true,
  },
};
