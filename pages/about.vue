<template>
  <v-row>
    <v-col
      sm="10"
      offset-sm="1"
      md="8"
      offset-md="2"
      xl="6"
      offset-xl="3"
    >
      <v-card>
        <v-card-text>
          <p>
            DataGit project is exploring the use of <a href="https://git-scm.com/">Git</a>,
            <a href="https://gitlab.com/">GitLab</a>, and
            <a href="https://git-lfs.github.com/">Git LFS</a> to organize public
            datasets.
          </p>
          <p>
            Goals are:
          </p>
          <ul>
            <li><strong>Collaboration</strong>: use of popular features for collaboration (<a href="https://docs.gitlab.com/ce/user/project/repository/forking_workflow.html">forks</a>,
              <a href="https://docs.gitlab.com/ce/user/project/merge_requests/">merge requests</a>, <a href="https://docs.gitlab.com/ce/user/project/issues/index.html">issues</a>, etc.) to
              organize and prepare datasets.</li>
            <li><strong>Full reproducibility</strong>: being able to version all changes to both data and metadata of a given dataset.</li>
            <li><strong>Machine readability</strong>: all datasets should be self-describing so that they can be automatically used.</li>
            <li><strong>Rich datasets</strong>: not just tabular datasets, but also datasets containing binary data (images, audio, video),
              graphs, relational data, datasets at various degrees of "cleanliness", etc.</li>
          </ul>
        </v-card-text>
        <v-card-title>
          <a href="#structure-of-datasets" class="anchor">
            <h2 id="structure-of-datasets">
              Structure of datasets
            </h2>
          </a>
        </v-card-title>
        <v-card-text>
          <p>DataGit datasets are simply dataset and metadata files in a
            Git repository with a consistent structure. Ideal DataGit repository contains:</p>
          <ul>
            <li><code>dataset</code>: the dataset itself<ul>
              <li><code>datapackage.json</code>: <a href="https://frictionlessdata.io/specs/data-package/">FrictionLess Data Package</a> metadata file, describing the dataset</li>
              <li><code>datasetDoc.json</code>: <a href="https://gitlab.com/datadrivendiscovery/data-supply">D3M dataset format</a> metadata file, describing the dataset</li>
              <li><code>schemaorg.jsonld</code>: a JSON-LD description of the dataset in <a href="https://schema.org/Dataset">schema.org Dataset schema</a></li>
            </ul></li>
            <li><code>README.md</code>: description of the repository and the dataset</li>
            <li><code>LICENSE</code>: copyright information of the dataset</li>
          </ul>
          <p>
            All data files should be stored in subdirectories of the <code>dataset</code> directory.
            Additional metadata files are allowed directly inside the <code>dataset</code> directory,
            and additional files of any kind are allowed outside of it.
          </p>
          <p>
            While this ideal is the goal, the path to it potentially leads through many
            commits and merge requests.
            So do not be shy if your repository contains just raw files you have.
            This is a starting point.
            Through collaboration the repository can be improved.
          </p>
          <p>
            We use GitLab project topics to annotate features and status of a dataset. Currently:
          </p>
          <ul>
            <li><code>in preparation</code>: dataset should not yet be used</li>
            <li><code>frictionless</code>: dataset is a FrictionLess Data Package</li>
            <li><code>d3m</code>: dataset is in D3M dataset format</li>
            <li><code>schemaorg</code>: dataset contains schema.org Dataset description</li>
            <li><code>tabular</code>: dataset is a tabular dataset</li>
            <li><code>csv</code>: dataset contains CSV file(s)</li>
          </ul>
          <p>
            More standard topics <a href="https://gitlab.com/data/meta">can be proposed</a>
            and additional non-standard topics are allowed.
          </p>
          <p>
            Repositories which are not maintained anymore, or have some uncorrectable issues,
            should be
            <a href="https://docs.gitlab.com/ce/user/project/settings/#archiving-a-project">archived</a>.
          </p>
        </v-card-text>
        <v-card-title>
          <a href="#how-to-download-a-dataset" class="anchor">
            <h2 id="how-to-download-a-dataset">
              How to download a dataset?
            </h2>
          </a>
        </v-card-title>
        <v-card-text>
          <p>
            You can <a href="https://git-scm.com/docs/git-clone"><code>git clone</code></a>
            the repository containing the dataset. You need
            <a href="https://git-lfs.github.com/">Git LFS</a> installed on your system
            for cloning to work correctly.
          </p>
          <p>
            You can browse the repository structure and files of the dataset
            through GitLab.com web interface.
          </p>
          <p>
            Some datasets can be large and/or can contain many files.
            If you do not care about all files, consider
            <a href="https://docs.gitlab.com/ce/topics/git/partial_clone.html">partial clone</a>,
            or clone with <code>GIT_LFS_SKIP_SMUDGE</code> environment variable
            set (which will make Git LFS files not be downloaded automatically), and then selectivelly
            download only those you want using <code>git lfs pull --include=path/to/file</code>.
            Or maybe you do not care about repository history. In that case you can use
            <a href="https://www.git-scm.com/docs/git-clone#Documentation/git-clone.txt---depthltdepthgt"><code>--depth</code></a> argument
            to limit the amount of history and perform a shallow clone.
          </p>
          <p>
            In the future <a href="https://gitlab.com/data/tools/issues/1">there will be a tool to help downloading multiple datasets, or even all
              of them</a>. Consider helping.
          </p>
        </v-card-text>
        <v-card-title>
          <a href="#contributing" class="anchor">
            <h2 id="contributing">
              Contributing
            </h2>
          </a>
        </v-card-title>
        <v-card-text>
          <p>
            This is an open project. You can <a href="#how-to-add-a-dataset">add new datasets</a>, or
            <a href="https://gitlab.com/data/meta">suggest them</a>.
            You can help making your or others' datasets closer
            to the <a href="#structure-of-datasets">ideal structure</a>, or just upload data you have,
            in the structure you have, and leave to others to help.
            You can help with <a href="https://gitlab.com/data/bots">development of existing bots</a> or create
            a <a href="#import-datasets-from-another-source">new one</a>.
            Forks and variations of datasets are welcome.
            (<a href="https://docs.gitlab.com/ce/user/project/repository/forking_workflow.html">Forking through GitLab user interface</a>
            keeps a link
            to the source dataset.) <a href="https://docs.gitlab.com/ce/user/project/merge_requests/">Merge requests</a>
            and <a href="https://docs.gitlab.com/ce/user/project/issues/index.html">opening issues</a> are welcome as well.
          </p>
        </v-card-text>
        <v-card-title>
          <a href="#how-to-add-a-dataset" class="anchor">
            <h2 id="how-to-add-a-dataset">
              How to add a dataset?
            </h2>
          </a>
        </v-card-title>
        <v-card-text>
          <ul>
            <li><a href="https://git-scm.com/docs/git-init">Initialize a new Git repository</a>: <code>git init new-dataset; cd new-dataset</code></li>
            <li><a href="https://github.com/git-lfs/git-lfs/blob/master/docs/man/git-lfs-install.1.ronn">Enable Git LFS inside the repository</a>: <code>git lfs install</code></li>
            <li>Copy dataset files into the repository, and <a href="#structure-of-datasets">structure</a> them if you can.</li>
            <li>Use <a href="https://gitlab.com/data/tools/blob/master/scripts/git-add.sh">this script</a> to mark files equal or larger than 100 KB
              for Git LFS.</li>
            <li><a href="https://git-scm.com/docs/git-add">Add all files</a>: <code>git add --all</code>, <code>git commit</code>.</li>
            <li><a href="https://docs.gitlab.com/ce/gitlab-basics/create-project.html">Create a GitLab.com project</a>.</li>
            <li>Push the local Git repository to GitLab.com.</li>
            <li><a href="https://docs.gitlab.com/ce/user/project/settings/#general-project-settings">Add topics to your GitLab.com project</a>.
              In addition to topics listed above,
              add also <code>datagit</code> and <code>dataset</code> topics. They are required for the project to be recognized as
              a DataGit dataset and indexed.</li>
            <li><a href="https://docs.gitlab.com/ce/user/project/members/share_project_with_groups.html#sharing-a-project-with-a-group-of-users">Add</a>
              <a href="https://gitlab.com/data/d">data/d</a> GitLab.com group with
              <strong>maintainer</strong> max access level to your project.
              You can see this action as "publishing" the dataset as a DataGit dataset.
              This is necessary so that dataset becomes easily discoverable. It
              also enables bots (e.g., the <a href="https://gitlab.com/data/bots/search">search index bot</a>)
              to access all information about the project (e.g.,
              <a href="https://gitlab.com/gitlab-org/gitlab/issues/38543">statistics</a>).
              Moreover, this gives the project backup access if any dataset gets abandoned
              by the maintainer.</li>
          </ul>
          <p>
            In the future there will be other ways to create datasets, e.g.,
            <a href="https://gitlab.com/data/meta/issues/5">creating
              a project directly under the data/d GitLab.com group</a>.
          </p>
          <a href="#git-repository-best-practices" class="anchor"><h3 id="git-repository-best-practices">Git repository best practices</h3></a>
          <ul>
            <li>Linear commit history without merge commits.</li>
            <li>Never force push and change history.</li>
            <li>All files smaller than 100 KB should be added directly to Git. Larger files should be added to Git LFS.
              In practice this makes a good balance between speed and storage (Git is fast at managing many small files,
              but Git LFS is better at large binary blobs).
              <a href="https://gitlab.com/data/tools/blob/master/scripts/git-add.sh">This script can help</a>.</li>
            <li>For compatibility with GitHub, all individual files should be smaller than 2 GB.
              <a href="https://gitlab.com/data/tools/blob/master/scripts/git-check.sh">You can use this script to check the repository</a>.</li>
          </ul>
        </v-card-text>
        <v-card-title>
          <a href="#import-datasets-from-another-source" class="anchor">
            <h2 id="import-datasets-from-another-source">
              Import datasets from another source
            </h2>
          </a>
        </v-card-title>
        <v-card-text>
          <p>We use <a href="https://gitlab.com/data/bots">bots</a> to import and structure existing dataset sources, instead of adding
            them manually. Moreover, bots run regularly to keep importing new datasets and update existing datasets when upstream dataset source
            changes.</p>
          <p>Different bots are able to structure datasets to a different degree, some still require manual work afterwards.
            Those bots make automatic merge requests when upstream datasets change, to help with porting of changes on top of manual changes.</p>
          <p>Consider helping developing more such bots or help with development of existing bots.
            Some potential sources you could help developing importing bots are listed
            <a href="https://gitlab.com/data/meta/issues?label_name%5B%5D=datasets+source">here</a>.</p>
        </v-card-text>
        <v-card-title>
          <a href="#known-limitations" class="anchor">
            <h2 id="known-limitations">
              Known limitations
            </h2>
          </a>
        </v-card-title>
        <v-card-text>
          <ul>
            <li>On GitLab.com, the maximum size of a repository is 10 GB. This includes
              Git objects and Git LFS files, over all commits (history). We currently skip
              such datasets, but <a href="https://gitlab.com/data/meta/issues/8">we are investigating alternatives</a>.</li>
          </ul>
        </v-card-text>
        <v-card-title>
          <a href="#questions-comments-suggestions" class="anchor">
            <h2 id="questions-comments-suggestions">
              Questions? Comments? Suggestions?
            </h2>
          </a>
        </v-card-title>
        <v-card-text>
          <p>Use <a href="https://gitlab.com/data/meta">meta</a> to
            discuss the project and report project-wide issues.</p>
        </v-card-text>
      </v-card>
    </v-col>
  </v-row>
</template>

<script>
  // @vue/component
  const component = {
    head() {
      return {
        title: 'About',
      };
    },
  };

  export default component;
</script>

<style lang="scss" scoped>
  h3 {
    margin-bottom: 16px;
  }

  ul {
    margin-bottom: 16px;
  }

  ul ul {
    margin-bottom: 0
  }

  a.anchor {
    text-decoration: none;
    color: inherit;
  }

  a.anchor:hover {
    h2:after, h3:after {
      display: inline-block;
      font: normal normal normal 24px/1 "Material Design Icons";
      text-rendering: auto;
      -webkit-font-smoothing: antialiased;
      -moz-osx-font-smoothing: grayscale;
      content: "\F337";
      padding-left: 4px;
      opacity: 50%;
      font-size: 80%;
      line-height: 80%;
    }

    h3:after {
      padding-left: 8px;
    }
  }
</style>
